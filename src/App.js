import React, {useEffect, useState} from "react";
import axios from "axios";
import {useDebounce} from "use-debounce";
import Google from './assets/images/google.png';
import Pagination from "./components/Pagination";
import Preloader from './assets/images/preloader.gif';

const App = () => {
    const [started, setStarted] = useState(false);
    const [loading, setLoading] = useState(false);
    const [result, setResult] = useState(null);
    const [show, setShow] = useState(false);
    const [value, setValue] = useState('');
    const [query] = useDebounce(value, 500);

    useEffect(() => {
        if (started) {
            if (query.trim()) {
                call().then(result => {
                    setResult(result);
                }).finally(() => {
                    setLoading(false);
                });
            } else {
                setResult(null);
                setLoading(false);
            }
        } else {
            document.getElementById('query-field').focus();
        }
    }, [query]);

    const handleChangeInput = event => {
        if (!started) {
            setStarted(true);
        }
        setValue(event.target.value);
    };

    const onPageChange = page => {
        call(page, true).then(result => {
            setResult(result);
        }).finally(() => {
            setLoading(false);
        });
    };

    const call = async (page = 1, show = false) => {
        setLoading(true);

        setShow(show);

        const {data} = await axios.get(`https://api.jikan.moe/v4/people?limit=25&page=${page}&q=${query}`);

        return data;
    };

    const display = user => {
        let name = user.name;

        let index = name.toLowerCase().search(query.toLowerCase());

        if (index !== -1) {
            name = name.toLowerCase().split(query.toLowerCase()).join(`<span class="highlighted">${query.toLowerCase()}</span>`);
        }

        return name;
    };

    return (
        <main className="main">
            <div className="icon">
                <img src={Google} alt="google"/>
            </div>
            <div className={`search-container${Boolean(result) && !show ? ' active' : ''}`}>
                <div className="search">
                    <input id="query-field" type="text" value={value} onChange={handleChangeInput}/>
                </div>
                {Boolean(result) && <div className="search-result">
                    {Boolean(result.data.length) && <>
                        <div className="items">
                            {result.data.slice(0, 10).map((user, index) => {
                                return (
                                    <a key={`user-header-${index}`} href={user.url} target="_blank" dangerouslySetInnerHTML={{__html: display(user) }} />
                                )
                            })}
                        </div>
                        <div className="more-info">
                            <button onClick={() => setShow(true)}>Show</button>
                            <span>Total: {result['pagination']['items']['total']}</span>
                        </div>
                    </>}
                    {!Boolean(result.data.length) && <div className="empty-data">
                        <p>No data for <span className="highlighted">{query}</span>.</p>
                    </div>}
                </div>}
            </div>
            {loading && <div className="preloader">
                <img src={Preloader} alt="preloader"/>
            </div>}
            {Boolean(result) && show && <div className="container">
                <Pagination
                    total={result['pagination']['items']['total']}
                    page={result['pagination']['current_page']}
                    change={onPageChange}
                    size="25"
                />
                {Boolean(result.data.length) && <div className="users">
                    {result.data.map((user, index) => {
                        return (
                            <div className="user" key={`user-${index}`}>
                                <div className="cover" style={{backgroundImage: `url(${user.images.jpg.image_url})`}}/>
                                <div className="info">
                                    <a href={user.url} target="_blank" dangerouslySetInnerHTML={{__html: display(user) }} />
                                </div>
                            </div>
                        )
                    })}
                </div>}
            </div>}
        </main>
    );
};

export default App;

import React from 'react';
import {usePagination, DOTS} from '../hooks/usePagination';

const Pagination = props => {
    const {
        size,
        page,
        total,
        change,
        sibling = 1
    } = props;

    const range = usePagination({
        size,
        page,
        total,
        sibling
    });

    if (page === 0 || range.length < 2) {
        return null;
    }

    const prev = () => {
        change(page - 1);
    };

    const next = () => {
        change(page + 1);
    };

    return (
        <div className="pagination">
            {page > 3 && <button onClick={prev}>Previous</button>}
            <ul className="pagination-content">
                {range.map((number, key) => {
                    if (number === DOTS) {
                        return <li key={`number-${key}`} className="pagination-item-dots">...</li>
                    }

                    return (
                        <li className={`pagination-item-page${number === page ? ' active' : ''}`}
                            onClick={() => change(number)}
                            key={`number-${key}`}
                        >{number}</li>
                    );
                })}
            </ul>
            {page !== range[range.length - 1] && <button onClick={next}>Next</button>}
        </div>
    );
};

export default Pagination;